import { OnInit, Component } from '@angular/core';
import { YouTubeService } from './youtube.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-youtube',
  templateUrl: 'youtube.html'
})
export class YouTubePage implements OnInit {
  header_data: any;
  private videoListArr = [];
  private title = [];
  private objArr;
  baseUrl: string = 'https://www.youtube.com/embed/';
  private homes = [];
  constructor(private sanitizer: DomSanitizer, public navCtrl: NavController, private _youtubeService: YouTubeService) {
    this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
  }
  ngOnInit() {
    this._youtubeService.getHome()
      .subscribe(response => this.homes.push(JSON.parse(JSON.stringify(response))),
      error => alert(error),
      () => {
        this.objArr = this.homes;
        console.log(this.objArr);
        this.objArr[0].items.forEach(element => {
          var url = element.snippet.resourceId.videoId;
          this.title.push(element.snippet.title);
          this.videoListArr.push({
            videoUrl: this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl + url),
            videoTitle: element.snippet.title,
            videoDesc: element.snippet.description,
            videoPublishedDate: element.snippet.publishedAt
          });

        });

      }
      )
  }

  //This function is to refresh the current page by pulling down
  doRefresh(refresher) {
    setTimeout(() => {
      this.ngOnInit();
      refresher.complete();
    }, 1000);
  }

}