import { Component } from '@angular/core';
import { Blog_Detail } from '../blog_detail/blog_detail.component';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
	templateUrl: 'blog.html',
	selector: 'page-blog',
})
export class BlogPage {
	url: string = 'http://dronepic.co.in/blog/wp-json/wp/v2/posts?_embed&per_page=99';
	//url: string = '../assets/blog.json';
	items: any;
	header_data: any;
	constructor(public navCtrl: NavController, private http: Http, private nav: NavController) {
		this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
	}

	ionViewDidEnter() {

		this.http.get(this.url)
			.map(res => res.json())
			.subscribe(data => {
				// we've got back the raw data, now generate the core schedule data
				// and save the data for later reference
				this.items = data;
			});
			
			}

	itemTapped(event, item) {
		this.nav.push(Blog_Detail, {
			item: item
		});
	}

	 //This function is to refresh the current page by pulling down
  doRefresh(refresher) {
    setTimeout(() => {
      this.ionViewDidEnter();
      refresher.complete();
    }, 1000);
  }
}