import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

declare var Marketcloud: any;

import '../../node_modules/marketcloud-js/dist/marketcloud.min';

@Injectable()
export class MarketcloudService {

  client:any;

  utils:any;

  constructor() {
    this.client = new Marketcloud.Client({
    	publicKey : '630fb49f-c23f-44d6-afac-d26aa01ff7cb'
    });

    this.utils = Marketcloud.Utils;

  }

}
