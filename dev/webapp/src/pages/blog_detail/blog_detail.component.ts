import { NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  templateUrl: 'blog_detail.html',
  selector: 'page-blog_detail',
})
export class Blog_Detail {
  selectedItem: any;
  header_data: any;
  constructor(private nav: NavController, navParams: NavParams) {
    this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
  }

}