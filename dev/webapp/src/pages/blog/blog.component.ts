import { Component } from '@angular/core';
import { Blog_Detail } from '../blog_detail/blog_detail.component';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
	templateUrl: 'blog.html',
	selector: 'page-blog',
})
export class BlogPage {
	url: string = 'http://dronepic.co.in/blog/wp-json/wp/v2/posts?_embed&per_page=99';
	//url: string = '../assets/blog.json';
	items: any;
	header_data: any;
	private element;
	private blogArr = [];
	private blogItems = [];
	constructor(public navCtrl: NavController, private http: Http, private nav: NavController) {
		this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
	}

	ionViewDidEnter() {

		this.http.get(this.url)
			.map(res => res.json())
			.subscribe(data => { this.items = data; },
			error => alert(error),
			() => {
				this.blogArr.push(this.items);
				console.log(this.blogArr[0]);
				for (this.element = 0; this.element < 4; this.element++) {
					this.populateData(this.element);
				}
			});

	}
	private populateData = function (element) {
		this.blogItems.push({
			blogTitle: this.blogArr[0][this.element].title.rendered,
			blogDescription: this.blogArr[0][this.element].excerpt.rendered,
			blogDate: this.blogArr[0][this.element].date,
			blogContent: this.blogArr[0][this.element].content.rendered,
			blogImage: this.blogArr[0][this.element]['_embedded']['wp:featuredmedia'][0].media_details.sizes.thumbnail.source_url
		})
		console.log(this.blogItems);
	}

	doInfinite(infiniteScroll) {
		console.log('Begin async operation');
		setTimeout(() => {
			console.log(this.blogArr[0].length);
			if (this.element > this.blogArr[0].length) {
				console.log("over");
			}
			else {
				for (var j = this.element; j < this.element + 1; j++) {
					if (j < this.blogArr[0].length) {
						this.populateData(j);
					}
				}
				this.element = j;
			}
			console.log('Async operation has ended');
			infiniteScroll.complete();
		}, 500);
	}




	itemTapped(event, item) {
		this.nav.push(Blog_Detail, {
			item: item
		});
	}

	//This function is to refresh the current page by pulling down
	doRefresh(refresher) {
		setTimeout(() => {
			this.ionViewDidEnter();
			refresher.complete();
		}, 1000);
	}
}