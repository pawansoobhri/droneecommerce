import { OnInit, Component } from '@angular/core';
import { YouTubeService } from './youtube.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-youtube',
  templateUrl: 'youtube.html'
})
export class YouTubePage implements OnInit {
  header_data: any;
  private videoListArr = [];
  private title = [];
  private objArr;
  private url;
  private element;
  baseUrl: string = 'https://www.youtube.com/embed/';
  private homes = [];
  constructor(private sanitizer: DomSanitizer, public navCtrl: NavController, private _youtubeService: YouTubeService) {
    this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
  }
  ngOnInit() {
    this._youtubeService.getHome()
      .subscribe(response => this.homes.push(JSON.parse(JSON.stringify(response))),
      error => alert(error),
      () => {
        this.objArr = this.homes;
        console.log(this.objArr);
        for (this.element = 0; this.element < 4; this.element++) {
          this.populateData(this.element);
        }
      })
  }

  //This function is to refresh the current page by pulling down
  doRefresh(refresher) {
    setTimeout(() => {
      this.ngOnInit();
      refresher.complete();
    }, 1000);
  }

  private populateData = function (element) {
    this.url = this.objArr[0].items[element].snippet.resourceId.videoId;
    this.title.push(this.objArr[0].items[element].snippet.title);
    this.videoListArr.push({
      videoUrl: this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl + this.url),
      videoTitle: this.objArr[0].items[element].snippet.title,
      videoDesc: this.objArr[0].items[element].snippet.description,
      videoPublishedDate: this.objArr[0].items[element].snippet.publishedAt

    });
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      if (this.element > this.objArr[0].length) {
        console.log("over");
      }
      else {
        for (var j = this.element; j < this.element + 1; j++) {
          if (j < this.objArr[0].items.length) {
            this.populateData(j);
          }
        }
        this.element = j;
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }
}
