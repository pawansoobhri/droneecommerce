import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';


import 'rxjs/add/operator/map';
@Injectable()
export class YouTubeService {
    //private _url:string="https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=UCDlpuesQBG7ufOKc1kLlv0g&key=AIzaSyDED1ua-y__04m9ROjoekXNw5FXzq7w6cc";
    private _playlistUrl: string = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=17&playlistId=UUDlpuesQBG7ufOKc1kLlv0g&key=AIzaSyDED1ua-y__04m9ROjoekXNw5FXzq7w6cc";
    constructor(private _http: Http) { }
    getHome() {
        return this._http.get(this._playlistUrl)
            .map((response: Response) => response.json() || {});

    }
}