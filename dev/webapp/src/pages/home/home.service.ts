import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeService {
  constructor(private http: Http) { }
  //private _dataListUrl:string="./assets/res.json";
  private _dataListUrl: string = "https://dronepic-app.herokuapp.com/feed.php";
  getUser() {
    return this.http.get(this._dataListUrl)
      .map((res: Response) => res.json() || {});
  }

}