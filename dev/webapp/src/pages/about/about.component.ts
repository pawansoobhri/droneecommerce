import { OnInit, Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { AboutService } from './about.service';

declare var window;

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage implements OnInit {
  header_data: any;
  private displayData = {};
  private aboutList;
  constructor(public navCtrl: NavController, private _AboutService: AboutService) {
    this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
  }

  ngOnInit() {
    this._AboutService.getUser().subscribe(data => this.displayData = JSON.parse(JSON.stringify(data))),
      error => alert(error),
      () => {
        this.aboutList = this.displayData;
        console.log("Component");
        console.log(this.displayData);
      }
  }

  contactUs(passednumber) {
    window.location = passednumber;
  }

}