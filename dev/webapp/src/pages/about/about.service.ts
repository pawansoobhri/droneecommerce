import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AboutService {
  private _dataListUrl: string = "https://dronepic-app.herokuapp.com/feed.php";
  //private _dataListUrl:string="./assets/res.json";
  constructor(private http: Http) { }

  getUser() {
    return this.http.get(this._dataListUrl)
      .map((res: Response) => res.json() || {});
  }

}