import { OnInit,Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FbService } from './fb.service';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Component({
    selector: 'page-fb',
    templateUrl: 'fb.html',
    providers: [FbService]
})
export class FbPage implements OnInit {
   
    public posts: Observable<any[]>;
    header_data: any;

    constructor(public navCtrl: NavController,
        public fbService: FbService) {
        this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };

        

    }
    ngOnInit() {

        this.posts = this.fbService
            .getPosts('dronepic.vimaan')
            .map(data => data.map(this.mapPosts));

    }

    

    mapPosts = (post) => {
        return {
            from: post.from,
            time: post.created_time * 1000, // convert to milliseconds
            message: post.message,
            photos: this.getPhotos(post),
            likes: post.likes.data.length,
        };
    }
    getPhotos = (post) => {
        if (!post.attachments)
            return [];

        let attachments = post.attachments.data[0].subattachments ||
            post.attachments;

        return attachments.data
            .filter(x => x.type == "photo")
            .map(x => x.media.image);
    }

     //This function is to refresh the current page by pulling down
  doRefresh(refresher) {
    setTimeout(() => {
      this.ngOnInit();
      refresher.complete();
    }, 1000);
  }


}