import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class FbService {

  private accessToken = 'EAASUZBI0m7K0BAF94n7GwWAPUhWxEVWHOLE92E9SpQzpUavZCDtt2N52NOnHChrP8Sr4zzsZBymir96rrzFWVdCN4xTclqbrRrf2faPMFRERdKr9QlyXZCCGRjt65djX8l7lcwRwfURZBdjikjwL6k8e0ZBcy0HQkZD';

  private graphUrl = 'https://graph.facebook.com/';
  private graphQuery = `?access_token=${this.accessToken}&date_format=U&fields=posts{from,created_time,message,attachments,likes}`;

  constructor(private http: Http) { }

  getPosts(pageName: string): Observable<any[]> {
    let url = this.graphUrl + pageName + this.graphQuery;

    return this.http
      .get(url)
      .map(response => response.json().posts.data);
  }
}