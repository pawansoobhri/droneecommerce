import { Component } from '@angular/core';

import { NavController, Platform } from 'ionic-angular';
import { MapService } from './map.service';


declare var window;
declare var google;

@Component({
    selector: 'page-map',
    templateUrl: 'map.html'
})
export class MapPage {
    header_data: any;
    private map;
    constructor(public navCtrl: NavController, private _MapService: MapService, private platform: Platform) {
        this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
        this.platform = platform;
        this.initializeMap();

    }

    initializeMap() {

        this.platform.ready().then(() => {
            let locationOptions = { maximumAge: 0, timeout: 10000, enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    let options = {
                        center: new google.maps.LatLng(0, 0),
                        // position.coords.latitude, position.coords.longitude),
                        zoom: 16,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                    this.map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    var latLong = new google.maps.LatLng(28.5240322, 77.0421308);
                    var marker = new google.maps.Marker({
                        position: latLong
                    });
                    marker.setMap(this.map);
                    this.map.setZoom(15);
                    this.map.setCenter(marker.getPosition());
                    console.log(position.coords.latitude, position.coords.longitude);

                },

                (error) => {
                    alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
                }, locationOptions

            );
        });
    }

    //This function is to refresh the current page by pulling down
    doRefresh(refresher) {
        setTimeout(() => {
            this.initializeMap();
        }, 1000);
    }
}
