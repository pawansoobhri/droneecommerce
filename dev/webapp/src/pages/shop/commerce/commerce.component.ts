import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { MarketcloudService } from '../../../providers/marketcloud-service';
import { CartPage } from '../components/cart/cart';
import { CategoriesPage } from '../components/categories/categories';
import { ProductsPage } from '../components/products/products';
import { UserPage } from '../components/user/user';

@Component({
    templateUrl: 'commerce.html'
})
export class CommercePage {
    userDetail: any;
    ProductsPage: any = ProductsPage;
    CategoriesPage: any = CategoriesPage;
    CartPage: any = CartPage;

    constructor(private marketcloud: MarketcloudService, public navCtrl: NavController) {
        console.clear();
        var isLogged = this.marketcloud.client.users.isAuthenticated();
        console.log(isLogged);
        (isLogged !== true) ? this.navCtrl.push(UserPage, {}) : this.getCurrentUserDetail();
    }
    getCurrentUserDetail = function () {
        this.marketcloud.client.users.getCurrent()
            .then((response) => {
                this.userDetail = response.data;
                console.log("commerce response");
                console.log(response);
                console.log(this.userDetail.name);
            })
            .catch((error) => {
                console.log(error);
            })
    }

}