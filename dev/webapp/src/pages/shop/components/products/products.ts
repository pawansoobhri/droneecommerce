import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { MarketcloudService } from '../../../../providers/marketcloud-service';
import { ItemPage } from '../item/item';

@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
  providers: [],
  entryComponents: [ItemPage]
})
export class ProductsPage {
  header_data: any;
  products: Array<any>;
  private stock_check;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private marketcloud: MarketcloudService,
    private alertCtrl: AlertController) {
    this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
    var promise: any;
    if (this.navParams.get('query')) {
      console.log("navPara: " + this.navParams);
      promise = marketcloud.client.products.list(this.navParams.get('query'));
    } else {
      promise = marketcloud.client.products.list();
    }

    promise
      .then((response) => {
        this.products = response.data;
        console.log("Market Cloud");
        console.log(this.products);

      })
      .catch((error) => {
        let alert = this.alertCtrl.create({
          title: 'Oops',
          subTitle: 'Unable to load products, please check your internet connection.',
          buttons: ['Ok']
        });
        alert.present();
      })
  }

  viewItemDetails(product) {
    this.navCtrl.push(ItemPage, {
      product: product
    })
  }


}
