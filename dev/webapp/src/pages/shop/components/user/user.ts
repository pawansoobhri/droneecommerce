import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { MarketcloudService } from '../../../../providers/marketcloud-service';
import { ProductsPage } from '../products/products';
import { CommercePage } from '../../commerce/commerce.component';
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
  providers: [],
})
export class UserPage {
  header_data: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private marketcloud: MarketcloudService,
    private alertCtrl: AlertController) {
    this.header_data = { ismenu: true, ishome: false, title: "DronEPIC" };
    var promise: any;
  }

  user = { name: '', mail: '', password: '' }
  createUser() {
    console.log(this.user)
    this.marketcloud.client.users.create({
      name: this.user.name,
      email: this.user.mail,
      password: this.user.password
    })
      .then((response) => {
        let alert = this.alertCtrl.create({
          title: 'User registered',
          subTitle: 'You have been succesfully registered',
          buttons: [
            {
              text: 'Ok',
              handler: data => {
                this.navCtrl.push(ProductsPage, {
                })
              }
            }]
        });
        alert.present();
      })
      .catch((error) => {
        console.log(error);
        let alert = this.alertCtrl.create({
          title: 'Please Check Again',
          subTitle: error.errors[0].type == "EmailAddressExists" ? error.errors[0].message : "Please enter the valid data",
          buttons: ['Ok']
        });
        alert.present();
      })
  }
  data = { user: { mail: '', password: '' } };
  authUser() {
    this.marketcloud.client.users.authenticate(this.data.user.mail, this.data.user.password)
      .then((response) => {
        console.log("user response");
        console.log(response);
        console.log("================================");
        let alert = this.alertCtrl.create({
          title: 'User registered',
          subTitle: 'You have been succesfully login',
          buttons: [
            {
              text: 'Ok',
              handler: data => {
                this.navCtrl.push(ProductsPage, {
                })
              }
            }]
        });
        alert.present();
      })
      .catch((error) => {
        console.log(error);
        let alert = this.alertCtrl.create({
          title: 'Please Check Again',
          //subTitle: error.errors[0].type == "EmailAddressExists" ? error.errors[0].message : "Please enter the valid data",
          buttons: ['Ok']
        });
        alert.present();
      })
  }
}







