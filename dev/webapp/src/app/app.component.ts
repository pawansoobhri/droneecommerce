import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { YouTubeService } from '../pages/youtube/youtube.service';
import { YouTubePage } from '../pages/youtube/youtube.component';

import { AboutPage } from '../pages/about/about.component';
import { AboutService } from '../pages/about/about.service';

import { HomePage } from '../pages/home/home.component';
import { HomeService } from '../pages/home/home.service';

import { MapPage } from '../pages/map/map.component';
import { MapService } from '../pages/map/map.service';

import { BlogPage } from '../pages/blog/blog.component';

import { FbPage } from '../pages/fb/fb.component';
import { FbService } from '../pages/fb/fb.service';

import { Blog_Detail } from '../pages/blog_detail/blog_detail.component';

import { Storage } from '@ionic/storage';

import { CartPage } from '../pages/shop/components/cart/cart';
import { ProductsPage } from '../pages/shop/components/products/products';
import { CategoriesPage } from '../pages/shop/components/categories/categories';
import { UserPage } from '../pages/shop/components/user/user';

import { CommercePage } from '../pages/shop/commerce/commerce.component';


import { MarketcloudService } from '../providers/marketcloud-service';
import { ConfigurationService } from '../providers/configuration-service';

@Component({
  templateUrl: 'app.html',
  providers: [YouTubeService, AboutService, HomeService, MapService, FbService, MarketcloudService],
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  marketcloudAppNamespace: string = 'DronEPIC';

  pages: Array<{ title: string, name: string, component: any }>;

  constructor(public platform: Platform,
    private configuration: ConfigurationService,
    private marketcloud: MarketcloudService,
    public storage: Storage,
    private alertCtrl: AlertController) {
    this.initializeApp();
console.clear();
    this.pages = [
      { title: 'DroneShop', name: 'globe', component: CommercePage },
      { title: 'Blog', name: 'book', component: BlogPage },
      { title: 'YouTube', name: 'logo-youtube', component: YouTubePage },
      { title: 'Facebook', name: 'logo-facebook', component: FbPage },
      { title: 'Map', name: 'pin', component: MapPage },
      { title: 'About', name: 'information-circle', component: AboutPage },

    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();

      //********************************
      this.storage.get(this.marketcloudAppNamespace + '_cart_id')
        .then((value) => {
          if (value === null) {
            this.marketcloud.client.carts.create({})
              .then((response) => {
                this.storage.set(this.marketcloudAppNamespace + '_cart_id', response.data.id)
                  .then(() => {
                    this.configuration.set('cart_id', response.data.id);
                  })
              })
              .catch((error) => {
                let alert = this.alertCtrl.create({
                  title: 'Oops',
                  subTitle: 'Unable to load categories, please check your internet connection.',
                  buttons: ['Ok']
                });
                alert.present();
              })
          } else {
            console.info("Using cart with id " + value);
            this.configuration.set('cart_id', value);
          }
        });
      //*****************************
    });
  }
  defaultPage(p) {
    this.nav.setRoot(HomePage);
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }


}
