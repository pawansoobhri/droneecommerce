import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { HeaderComponent } from '../modals/header/header.component';
import { YouTubePage } from '../pages/youtube/youtube.component';
import { TabsPage } from '../pages/tabs/tabs.component';
import { AboutPage } from '../pages/about/about.component';
import { HomePage } from '../pages/home/home.component';
import { BlogPage } from '../pages/blog/blog.component';
import { Blog_Detail } from '../pages/blog_detail/blog_detail.component';
import { FbPage } from '../pages/fb/fb.component';
import { Facebook } from 'ionic-native';
import { MapPage } from '../pages/map/map.component';
import { ProductsPage } from '../pages/shop/components/products/products';
import { CategoriesPage } from '../pages/shop/components/categories/categories';
import { ItemPage } from '../pages/shop/components/item/item';
import { CartPage } from '../pages/shop/components/cart/cart';
import { ConfigurationService } from '../providers/configuration-service';
import { Storage } from '@ionic/storage';
import { CommercePage } from '../pages/shop/commerce/commerce.component';
import { UserPage } from '../pages/shop/components/user/user';
 
@NgModule({
  declarations: [
    MyApp,
    YouTubePage,
    AboutPage,
    TabsPage,
    HomePage,
    HeaderComponent,
    MapPage,
    BlogPage,
    Blog_Detail,
    FbPage,
    ProductsPage,
    CategoriesPage,
    ItemPage,
    CartPage,
    CommercePage,
    UserPage 


  ],
  imports: [HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    AboutPage,
    YouTubePage,
    HomePage,
    HeaderComponent,
    MapPage,
    BlogPage,
    Blog_Detail,
    FbPage,
    ProductsPage,
    CategoriesPage,
    ItemPage,
    CartPage,
    CommercePage,
    UserPage


  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }, Facebook,
    ConfigurationService,
    Storage]
})
export class AppModule { }
