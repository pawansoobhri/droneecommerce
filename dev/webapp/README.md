Ionic 2 App Base
=====================

This is the base template for Ionic 2 starter apps.

## Using this project

You'll need the Ionic CLI with support for v2 apps:

```bash
$ npm install -g ionic@beta
```

Then run:

```bash
$ ionic start AppName --v2 --ts
```
$ cd AppName
$ ionic serve OR 
$ ionic serve -l for all platforms


More info on this can be found on the Ionic [Getting Started](http://ionicframework.com/docs/v2/getting-started/) page.
